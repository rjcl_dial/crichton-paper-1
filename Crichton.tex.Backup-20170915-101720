%&latex
\documentclass{elsart}
\usepackage{natbib}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}

\begin{document}

%\runauthor{Clement et al.}
\begin{frontmatter}
\title{Field-scale carbon exchange estimation over a harvest cycle for a  managed pasture in Southwest Scotland }
\author[UoE]{Clement, R}
\author[York]{Toet, S}
\author[York]{Keane, B}
\author[Kew]{Lee, M}
\author[SRUC]{Rees, B}
\author[Germany]{Blei, E}
\author[UoE]{ Williams, M}


\address[UoE]{School of GeoScience, University of Edinburgh}
\address[Kew]{Kew Gardens, Kew}
\address[SRUC]{SRUC, Edinburgh}
\address[York]{Dept of Env Sciences, York University}
\address[Germany]{Gottingen University}

\begin{abstract}



Accurate measurement of carbon exchange requires cross validation and a method to account for variability not captured by the measurement systems

Desired to identify spatial variability of fluxes at typical UK managed pasture. 

Used multiple eddy covariance systems consisting of both standard and low cost systems.  

Identified and rectified factors that were caused of major discrepancies between systems

Measured in co-located and dispersed deployments

Something about processing/QC

No significant discrepancies between flux measurement locations despike know differences in soil moisture (and canopy?)

Error estimate of fluxes - assuming variability caused by stochastic atmospheric processes

Underestimation of fluxes based on energy budget closure (and carbon?) observed for all flux systems.  Cause not yet known.
 
\end{abstract}

\begin{keyword}
trace gas fluxes, pasture, eddy covariance
\end{keyword}

\end{frontmatter}

\section{Introduction}
\cite{Peichl2011}
observed NEE not to change with driver variability - on the annual time scale.


Pasture flux measurements?

Spatial variability in flux measurements? landcover 
(Fischer)

driven by topogrpahy/soil texture (Burke, hook and burke, Risch), plants (El Madany)

Effect of fertilization on flux measurements (Kirschbaum)?

Effect of harvesting on pasture measurements?

\section{Methods}

\subsection{Experiment site}

The experiment was conducted at the Crichton, Dumfries Scotland ( 55$^{\circ}$ 2' 34"N,   35$^{\circ}$ 35' 1" W).  .    The field employed for this experiment was a 6 Ha managed pasture planted to rye grass  (Lolium) since ????.   The field is managed by SRUC with typical treatments  including grazing, fertilizer applications in the form of slurry and chemical fertilizers, and a sequence of harvesting events during the growing season.    SRUC has been conducting agricultural based research at the Crichton since ????, providing a long history of information on site management.  Management events that occurred during the period of this experiment are listed in table ???

\begin{tabular}{|c|c|}\hline
Date & Management \\\hline
4/3/2014 & slurry application,  30 m$^3$ ha$^{-1}$ \\\hline
12/3/2014 & urea application,  50.6 kg N ha$^{-1}$\\\hline
14/5/2014 to 16/5/2014  & harvesting, 150 tons \\\hline
21/5/2014 & slurry application,  30 m$^3$ ha$^{-1}$ \\\hline
3/6/2014 & urea application,  50.6 kg N ha$^{-1}$ \\\hline
10/6/2014 & herbicide application \\\hline
28/6/2014 to 29/6/2014   & harvesting, 94 tons\\\hline
7/7/2014 & slurry application,  30 m$^3$ ha$^{-1}$ \\\hline
11/8/2014 & harvesting 110 tons \\\hline
9/9/2014 to 12/9/2014   &  grazing of 92 cows \\\hline
12/9/2014 to 24/9/2014 & grazing of 22 heifers \\\hline
10/10/2014 to 20/11/2014 & grazing of 22 heifers \\\hline
\end{tabular}?  


\subsection{Vegetation and soil measurements}

Intermittent measurements of soil moisture, canopy biomass, leaf area and canopy height were made on a grid of sampling points covering the entire experimental field.
  The sampling grid was established in the spring of 2015.  The sampling points, with a nominal spacing of 20 meters were marked with permanent location markers, which were later surveyed using a  total station survey system (Leica ???) referenced to a GPS location.
 The sample point survey data was also used to obtain a detailed topographic map of the field site.
 
Soil moisture was measured to a depth of approximately 6 cm using a hand held moisture probe (Delta T,  Theta).   Nominally  three samples were collected from a random location collocated with each survey sampling point.  These samples were combined to give a mean and uncertainty of soil moisture for the survey sample locations for each date of soil moisture sampling.  ??? Soil moisture maps for other dates were obtained using interpolations constrained by temporally continuous soil moisture measurements collected at three locations and several depths . Dates of soil moisture surveys are given in table \ref{table:exp_dates}.

Vegetation leaf area index (LAI) and biomass estimates were measured at the sampling  grid locations on the dates specified in table  \ref{table:exp_dates}.
 Leaf area index values were collected using an LAI meter (LI2000, Licor Inc.).  Additional destructive measurements of LAI were collected on ???  in order to determine specific leaf areas for relating biomass to leaf area .  Biomass estimates were collected at the sample grid points on ??? dates (see table \ref{table:exp_dates}).  These non-destructive biomass estimates were obtained using a rising plate meter (??? make model) which was calibrated against destructive sampling on ???date???.     

\begin{figure}[!ht]
  \centering
     \includegraphics[width=\textwidth]{figures/measurement_locations}
  \caption{Measurement locations for Crichton experiment.   Filled red circles indicate permanent or deployment flux tower locations, open red circles are inter-comparison flux tower locations, yellow markers indicate microclimate measurement sites (square UoE, triangle SRUC, circle CEH), blue dots are soil and vegetation survey sampling points and the green diamonds indicate flux chamber measurement locations.  The strip of chamber locations near the top of the field are the chambers associated with the Sky-line chamber system, which has associated soil measurements.  }
\label{fig:locations}
\end{figure}

\begin{table}[h]
\caption{Microclimate measurement details}
\label{table:microclimate}
\begin{tabular}{|c|c|}\hline
Measurement & Location(s) \\\hline
short/long wave - incoming/outgoing & B\\\hline
incoming PAR & B\\\hline
outgoing PAR & D,F\\\hline
Diffuse PAR & D  \\\hline
Net Radiation  & B,D,F\\\hline
Air T/RH & B,2,4  \\\hline
Soil T & 1,2,3,4,5 \\\hline
Soil moisture & 1,2,3,4,5  \\\hline
Precipitation &  1,5\\\hline
Wetness &  B\\\hline
Pressure &  A\\\hline
\end{tabular}
\end{table}




\begin{table}[h]
\caption{Measurement related dates}
\label{table:exp_dates}
\begin{tabular}{|c|c|}\hline
Date & Measurement \\\hline
5/2014 & Installation of permanent flux systems and microclimate sensors (A and B)\\\hline
3/2015 & Soil moisture survey \\\hline
4/2015 & Installation  of intercomparison flux systems (D-G)\\\hline
a & Soil moisture survey  \\\hline
6/2015 & Relocation   of deployed flux systems (D-G) \\\hline
a & Soil moisture survey  \\\hline
a & Canopy biomass survey  \\\hline
a & LAI survey  \\\hline
a & Soil moisture survey  \\\hline
a & Remove   deployed flux systems (C-G)  \\\hline
a &  xxx \\\hline
\end{tabular}
\end{table}


\subsection{Microclimate measurements}
Microclimate measurements were collected collected both at the microclimate measurement locations (yellow markers in figure \ref{fig:locations}) or in association with the eddy covariance towers (red markers in figure \ref{fig:locations}).  The details of these measurements and their locations are given in table \ref{table:microclimate}. 
Microclimate measurements were recorded at  0.1 Hz or 10 Hz if associated with a eddy covariance system.
 In addition to these measurements, wind speed, direction, air temperature, H2O and CO2 concentrations were available from the eddy covariance stations.
    
       
\subsection{Eddy covariance flux measurements and processing}
     
Nine CO$_2$ flux measurement systems were deployed during the field experiment (see table \ref{table:flux}).
 Two of these systems (A,B) were installed over the period of the experiment while most were operational only during the intensive 2015 campaign period (see table \ref{table:exp_dates} ).  Five of the trace gas monitors consisted of standard sensors designed for use in eddy covariance systems.  The other four systems incorporated low cost IRGAs for the measurement of CO2 and humidity chips for the measurement of water vapour (see Hill et al. ???).
Data were recorded at 10 Hz.  All data were cross compared (both eddy covariance and microment) to identify any signal biases.  Raw data were screened for spikes and plausible values.   Both the humidity chip and GMP343 IRGA measurements were corrected for environmental dependencies (RH chip temperature and pressure effects (Honeywell ???), GMP343 temperature and pressure). Velocity measurements were rotated to minimize the mean vertical velocity.     A site specific cospectral model (Massman ???) was developed, based on sensible heat fluxes. Similarly, sensor specific models of sensor frequency response attenuation were developed and combined with the cospectral models to determine, and then apply, frequency response corrections. The open path sensor fluxes was also corrected for density effects (WPL ???).  The resulting fluxes were screened for plausibility.\  

\begin{table}
\caption{Eddy covariance towers and installed  flux systems}
\label{table:flux}     
\begin{tabular}{|c|c|p{6cm}|c|}\hline
Tower & Sonic & Trace gas sensors & height \\\hline
A & CSAT4 & EC150   (CO$_2$/H$_2$O)  & 2.5 m\\\hline
B & Gill R3 & LI7000 (CO$_2$/H$_2$O)  & 11.0 m\\\hline
C & RM  Young& LGR ???(CO$_2$/CH$_4$/H$_2$O)  & 8 m\\\hline
D & Gill R3  & LI7500 enclosed/fan (CO$_2$/H$_2$O) & 4.5 m\\\hline
E & Gill R3  & LI7500 enclosed/fan (CO$_2$/H$_2$O)\newline GMP/RH fan (CO$_2$/H$_2$O) \newline GMP/RH pump (CO$_2$/H$_2$O) & 4.5 m\\\hline
F & Gill R3  &GMP/RH fan (CO$_2$/H$_2$O) & 4.5 m\\\hline
G & Gill R3  &GMP/RH fan (CO$_2$/H$_2$O) & 4.5 m\\\hline
\end{tabular}
\end{table}




\subsection{Chamber measurements and processing}
Three chamber flux measurement methodologies were employed during this experiment, these chamber systems will be referred to as SRUC, LRC, and Skyline.

The Skyline chamber system was an automated, transparent, static chamber which was used to measure fluxes of CO2, CH4, and N2O.  Measurements were made every meter along a 26 m transect (see figure \ref{fig:locations}).  Each chamber measurement took $\approx$ 8 minutes, so a complete cycle of all chambers could be completed in $\approx$ 3.75 hours.  Soil temperature, soil moisture, and internal and external PAR were measured during each chamber measurement. For each chamber measurement, the CO2 uptake/release rate was calculated from the concentration measurements using either linear or exponential curve fits - depending upon the non-linearity of the curve suggesting saturation/limitation of the release/uptake rates.  

The SRUC chambers were manual, opaque, static chamber, from which dark CO2 respiration rates could be determined. The sampling locations were randomly distributed across the field (see figure ) but were not coincident with the Skyline chamber locations. Measurements were collected during mid-day, approximately ??? times per week.  T.  Gas samples were taken every ??? minutes following chamber placement.  Fluxes were determined from the     

The LRC chamber was a manual, transparent, static chamber  used to determine net ecosystem exchange of CO2.  The sampling locations included a subset of those used by both the Skyline and SRUC chambers.  Measurements were collected on four days during daylight hours. For each chamber, a set of measurements were collected for a range of chamber PAR levels.  The PAR levels were manipulated by imposing increasing levels of shading of the chamber using mesh netting.   
Fluxes were determined from the linear CO2 concentration vs time curve obtained by measuring (LI6000, Licor Inc) the chamber CO2 concentration every five seconds for the minute following chamber closure.  

\section{Results}
\subsection{Site characteristics}
The experimental site is located at an elevation of $\approx$ 50 m on the south-west facing flank of a NW to SE oriented ridge having an elevation of about 75 meters (figure \ref{fig:topo}). For the predominant, southwesterly, wind direction (figure \ref{fig:climate}) the upwind fetch topography includes about 500 m of the ridges lower slopes and bout 2 to 4 km of flat valley bottom.  The regions land use is primarily agricultural/pastoral. 

\begin{figure}[!ht]
  \centering
     \includegraphics[width=\textwidth]{figures/topo.png}
  \caption{Topography of experiment site  }
  \label{fig:topo}
\end{figure}

The site climatology of temperature, and precipitation for the year surrounding the campaign were similar to long term climatology obtained from weather service data (figure \ref{fig:climate}).  The wind climatology (figure \ref{fig:climate})  for the site was roughly in line with other regional wind climatologies (not shown).  However, this site's winds appear to show minimum wind probabilities for  northerly and south-southwesterly winds.  This pattern is likely caused by higher topography upwind  for those wind directions (figure \ref{fig:topo}), and may indicate the potential for topographic flow distortion, acceleration and/or deceleration, with associated impacts on flux advection terms.

\begin{figure}[!ht]
  \centering
     \includegraphics[width=\textwidth]{figures/climate.png}
  \caption{Site climatology of temperature, precipitation and wind. The dashed lines represent long-term weather service values and solid lines are site measured values.  The temperature plot shows monthly mean values (mean daily max and min for weather service data).  The precipitation plot shows monthly totals.  The wind rose plots show the probability density of wind by direction and speed - darker values indicate higher probability.}
  \label{fig:climate}
\end{figure}

\textbf{ NO RESULTS FOR SOIL PROPERTIES?
}\subsection{Eddy covariance flux system comparison \\ } 



The eddy covariance flux systems were \subsection{Temporal patterns}
wind speed and direction

biomass, canopy height, and leaf area

precip,soil moisture and humidity

Radiation, air and soil temperature

Fluxes 

\subsection{Evidence of spatial variability}
soil moisture

Radiation

soil temperature

vegetation

Fluxes
flux footprints

\subsection{Modelling ecosystem responses}
respiration rate
light response curves

scaling to the site

resultant spatial variability

Comparison with harvest data.


\begin{figure}[!ht]
\includegraphics[width=\textwidth]{figures/image001.png}
\end{figure}



\begin{figure}[!ht]
  \centering
     \includegraphics[width=\textwidth]{figures/flux_time_series.png}
  \caption{Sample of fluxes during campaign period. Top chart C02 flux, center chart latent heat flux, bottom chart sensible heat flux.  }
\end{figure}


\begin{figure}
    \centering
    \begin{subfigure}[b]{0.4\textwidth}
        \includegraphics[width=\textwidth]{figures/footprint_day.png}
        \caption{diurnal footprint}
        \label{fig:FPday}
    \end{subfigure}
    \begin{subfigure}[b]{0.4\textwidth}
        \includegraphics[width=\textwidth]{figures/footprint_night.png}
        \caption{nocturnal footprint}
        \label{fig:FPnight}
    \end{subfigure}
\end{figure}



\section{Discusion}


\bibliographystyle{elsart-harv}
\bibliography{Crichton}


% 
\end{thebibliography}


\appendix
\section{APPENDIX}

\begin{equation}
a + b = c
  \label{foo}
\end{equation}

\end{document}

